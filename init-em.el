(defun em/buffer ()
  (interactive)
  (get-buffer-create "*em*"))
(defun em/info (msg)
  (interactive "sInfo message to output:")
  (with-current-buffer (em/buffer)
    (insert (format "INFO - %s\n" msg))))
(defun em/warn (msg)
  (interactive "sWarn message to output:")
  (with-current-buffer (em/buffer)
    (insert (format "WARN - %s\n" msg))))
(defun em/erro (msg)
  (interactive "sError message to output:")
  (with-current-buffer (em/buffer)
    (insert (format "ERR  - %s\n" msg))))
(defun em/readvars ()
  ""
  "Loop over text returning key/values split by = "
  ""
  (interactive)
  (let ((values (list)))
    (mapcar (lambda (line)
              (when line (let ((sp (split-string line "=")))
                           (if (= (length sp) 2)
                               (add-to-list 'values `((,(pop sp) . ,(pop sp))))))))
            (split-string
             (buffer-substring-no-properties
              (point-min)
              (point-max))
             "\n")) values))

(defun em/read-hashs ()
  ""
  "Fetch shas from files"
  ""
  (interactive)
  (let ((ohash (with-temp-buffer (insert-file-contents "bundles.sha")
                                 (em/readvars))))
    (message "%S" ohash) ohash))

(defun em/create-directory-if-not-exists (new-directory-name)
  (if (not (file-directory-p new-directory-name))
      (if (y-or-n-p (concat "Create Directory" new-directory-name))
          (progn (make-directory new-directory-name)))))

;; test if settinngs file has changed
(defun em/settings-changed ()
  (interactive)
  (let ((ohash (with-temp-buffer (insert-file-contents "hash.sha")
                                 (buffer-string)))
        (chash (shell-command-to-string "sha256sum settings.org")))
    (not (string-equal ohash chash))))
(defun em/settings-update-hash ()
  (interactive)
  (shell-command "sha256sum settings.org > hash.sha"))

;;(if (em/settings-changed) (em/settings-update-hash))
;;(message "%s" (em/settings-changed))

(defun em/load-file-if-exists (name)
  (if (file-exists-p name)
      (progn (load-file name)
             (message "Loaded file %s" name))))

(defun em/narrow-or-widen-dwim (p)
  "Widen if buffer is narrowed, narrow-dwim otherwise.
Dwim means: region, org-src-block, org-subtree, or
defun, whichever applies first. Narrowing to
org-src-block actually calls `org-edit-src-code'.

With prefix P, don't widen, just narrow even if buffer
is already narrowed."
  (interactive "P")
  (declare (interactive-only))
  (cond ((and
          (buffer-narrowed-p)
          (not p))
         (widen))
        ((region-active-p)
         (narrow-to-region (region-beginning)
                           (region-end)))
        ((derived-mode-p 'org-mode)
         ;; `org-edit-src-code' is not a real narrowing
         ;; command. Remove this first conditional if
         ;; you don't want it.
         (cond ((ignore-errors (org-edit-src-code) t)
                (delete-other-windows))
               ((ignore-errors (org-narrow-to-block) t))
               (t (org-narrow-to-subtree))))
        ((derived-mode-p 'latex-mode)
         (LaTeX-narrow-to-environment))
        (t (narrow-to-defun))))

(defun em/set-mark-line ()
  (interactive)
  (beginning-of-line)
  (set-mark-command nil)
  (forward-line))

(defun em/set-mark-line-start ()
  (interactive)
  (beginning-of-line)
  (set-mark-command nil))

(defun em/set-mark-first-char ()
  (interactive)
  (back-to-indentation)
  (set-mark-command nil))

;;ask for input if interactively called
(defun em/verify-binary (bin)
  (interactive "sBinary to check for:")
  (let ((bin-path (executable-find bin)))
    (if bin-path (em/info (format "Found binary for %s located %s" bin bin-path))
      (em/warn (format "Missing %s not in system path" bin)))))
