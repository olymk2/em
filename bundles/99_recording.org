#+TITLE: Screen recording

* Package setup
** Ubuntu / Debian Setup
#+BEGIN_SRC shell :tangle apt-ubuntu.sh :results silent
sudo apt-get install asciinema scrot gifsicle
#+END_SRC
** Solus Setup
#+BEGIN_SRC shell :tangle eopkg-solus.sh :results silent
eopkg install asciinema scrot gifsicle
#+END_SRC

* Screen recording

#+BEGIN_SRC emacs-lisp :results silent  
  (em-loader-verify-binary "asciinema")
  (em-loader-verify-binary "asciinema2gif")
  (em-loader-verify-binary "scrot")
  (use-package gif-screencast :straight t)
(setq gif-screencast-args '("--quality" "25" "--focused"))
#+END_SRC
