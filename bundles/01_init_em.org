#+TITLE: Init EM

* Global Vars
#+BEGIN_SRC emacs-lisp :results silent  
(setq ob-project-template-folder "~/em/org/project/templates/")
#+END_SRC

* File handling
Needs function from em-loader library
#+BEGIN_SRC emacs-lisp :results silent
(defun em-load-file-if-exists (name)
  (if (file-exists-p name)
      (progn (load-file name)
             (em-loader-info (format "Loaded file %s" name)))))


(defun em-create-directory-if-not-exists (new-directory-name)
    (if (not (file-directory-p new-directory-name))
        (if (y-or-n-p (concat "Create Directory" new-directory-name))
            (progn
              (make-directory new-directory-name)))))
#+END_SRC

* Package management
Pull in auto package update so we can run updates with out opening a buffer.
#+BEGIN_SRC emacs-lisp :results silent
(use-package auto-package-update :straight t)
#+END_SRC
