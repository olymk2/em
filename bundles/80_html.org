
#+BEGIN_SRC emacs-lisp :results silent
  (em-hydra hydra-web "EM - Web"
     ("Actions" (
    ("f" web-beautify-html "Format buffer"))))

  (em-hydra hydra-html "EM - Html"
          ("Actions" (
    ("f" web-beautify-html "Format buffer"))))
#+END_SRC


