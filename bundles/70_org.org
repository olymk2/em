#+TITLE: Custom org config


* Requirements
** Ubuntu requirements
#+BEGIN_SRC shell :tangle apt-ubuntu.sh :results silent
sudo apt install texlive-all graphviz imagemagick
#+END_SRC

** Solus requirements
#+BEGIN_SRC shell :tangle eopkg-solus.sh :results silent
# latex requirements
eopkg install texlive-all graphviz imagemagick
#+END_SRC

** Check availability
#+BEGIN_SRC emacs-lisp :results silent
  (em-loader-verify-binary "pdflatex")
  (em-loader-verify-binary "convert")
  (em-loader-verify-binary "dot")
#+END_SRC
org-set-startup-visibility
* Org mode

#+BEGIN_SRC emacs-lisp :results silent
(setq org-roam-v2-ack t)

(use-package "htmlize" :straight t)
(use-package dot-mode :straight t)
(use-package poporg :straight t)
(use-package verb :straight t)
(use-package org-roam :straight t)
;;(use-package literate-calc-mode :straight t)
(use-package ob-async :straight t)
(setq org-startup-folded t)
;(setq org-latex-pdf-process (list "latexmk -pdf -bibtex %f"))
(setq org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

(setq org-startup-with-inline-images t)
(setq org-indent-indentation-per-level 1)
(setq org-src-preserve-indentation t)
(setq org-src-tab-acts-natively t)
(setq org-image-actual-width nil)
(setq org-fill-paragraph t)
'(org-file-apps
  (quote
   ((auto-mode . emacs)
    ("\\.mm\\'" . default)
    ("\\.x?html?\\'" . "/usr/bin/firefox %s")
    ("\\.pdf\\'" . default))))

(setq org-roam-directory (concat user-emacs-directory "notes"))

;;(use-package helm-org-rifle :straight t)
(add-hook 'org-mode-hook 'turn-on-visual-line-mode)
#+END_SRC
** Org extensions
#+BEGIN_SRC emacs-lisp :results silent
  (use-package toc-org :straight t)
  (if (require 'toc-org nil t)
      (add-hook 'org-mode-hook 'toc-org-enable)
    (warn "toc-org not found"))
#+END_SRC
** Org languages
#+BEGIN_SRC emacs-lisp :results silent
      (use-package redis :straight t)
      (use-package ob-go :straight t)
(use-package ob-graphql :straight t)
      (use-package ob-restclient :straight t)
      (use-package ob-http :straight t)
      (use-package ob-async :straight t)
      (use-package company-restclient :straight t)

      ;;(use-package ob-sql :straight t)
      (use-package plantuml-mode :straight t)
      (setq org-odt-preferred-output-format "docx")
      (setq org-babel-python-command "python3")
      ;;(require 'ob-shell)
      (require 'ob-python)
      (org-babel-do-load-languages
       'org-babel-load-languages
       '((python . t)
         (go . t)
         (clojure . t)
         ;; (clojurescript . t)
         (dot . t)
         (sql . t)
         (plantuml . t)
         (latex . t)
         (lilypond . t)
;         (sqlite . t)
         (restclient . t)
         (shell . t)
         ))
      ;; enable babel rectclient, run php code
      ;;(add-to-list 'org-babel-load-languages '(php . t))
      ;;(org-babel-do-load-languages 'org-babel-load-languages '((restclient . t)))
  (setq org-preview-latex-default-process 'imagemagick)
  (setq org-src-fontify-natively t)
  (add-to-list 'company-backends 'company-restclient)

#+END_SRC

* Export customization
#+begin_src emacs-lisp :results silent
    (setq org-file-apps
      '((system . "/usr/bin/xdg-open %s")
        ("\\.docx\\'" . default)
        ("\\.ods\\'" . system)
        ("\\.odt\\'" . system)
        ("\\.mm\\'" . default)
        ("\\.x?html?\\'" . "firefox %s")
        ("\\.pdf\\'" . default)
        (auto-mode . emacs)))
#+end_src

* Latex customization
#+BEGIN_SRC emacs-lisp :results silent
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-src-block-backend 'minted)
(setq org-format-latex-options (plist-put org-format-latex-options :scale 4.0))
#+END_SRC
* Easy templates
Nice explanation below.
http://nicholasvanhorn.com/posts/org-structure-completion.html

#+BEGIN_SRC emacs-lisp :results silent
  (add-to-list
   'org-structure-template-alist
   '("sq" . "src sql"))

  (add-to-list
   'org-structure-template-alist
   '("sc" . "src clojure"))

  (add-to-list
   'org-structure-template-alist
   '("sl" . "src emacs-lisp :results silent"))

  (add-to-list
   'org-structure-template-alist
   '("sp" . "src python :results silent"))

  (add-to-list
   'org-structure-template-alist
   '("ss" . "src shell :results silent"))



#+END_SRC

* Capture
** Settings
#+BEGIN_SRC emacs-lisp :results silent
(setq org-directory (concat user-emacs-directory "org/"))
(setq org-default-notes-file (concat org-directory "notes.org"))
(setq org-default-refile-file (concat org-directory "refile.org"))
(setq org-agenda-files (concat org-directory "agenda.org"))
(setq org-refile-targets '((org-default-notes-file :maxlevel . 3)))

#+END_SRC
** Templates

#+BEGIN_SRC emacs-lisp :results silent
  (setq org-capture-todo-headline "Default todo's list")
  (defun org-find-header ()
    (goto-char (point-min))
    (unless (search-forward (concat "* " org-capture-todo-headline) nil nil nil)
      (insert (concat "* " org-capture-todo-headline))))
  (setq org-capture-templates
        '(("t" "Todo" entry (file+function org-default-refile-file org-find-header)
           "* TODO %?\n%U" :empty-lines 1)
          ("T" "Todo with Clipboard" entry (file+function org-default-refile-file org-find-header)
           "* TODO %?\n%U\n   %c" :empty-lines 1)
          ("n" "Note" entry (file+function org-default-refile-file org-find-headline)
           "* NOTE %?\n%U" :empty-lines 1)
          ("p" "Priority" entry (file+headline org-default-refile-file "Priorities" )
           "* TODO %?\n%U" :empty-lines 1)
          ("N" "Note with Clipboard" entry (file+function org-default-refile-file org-find-headline)
           "* NOTE %?\n%U\n   %c" :empty-lines 1)))
#+END_SRC

#+BEGIN_SRC emacs-lisp :results silent
    (defun show-org-capture ()
    (interactive)
      (find-file org-default-refile-file)
    (outline-show-subtree)
  )
#+END_SRC

* Presentations

#+BEGIN_SRC emacs-lisp :results silent
(use-package ox-reveal :straight t)
(setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
#+END_SRC


#+BEGIN_SRC emacs-lisp :results silent
(setq restclient-log-request t)
#+END_SRC

* Org project templates

#+BEGIN_SRC emacs-lisp :results silent
    (add-to-list 'org-export-backends 'md)

    (setq-default ob-projects-folder "~/repos/")
    (setq-default ob-project-template-folder (concat em-loader-user-emacs-directory "org/project/templates/"))
    (use-package ob-project-template :straight (ob-project-template 
              :host gitlab 
              :repo "emacs-open-source-library-collection/emacs-ob-project-templates" 
              :branch "master"))
#+END_SRC

* Hydras

#+BEGIN_SRC emacs-lisp :results silent


  (em-hydra hydra-org "EM - Org mode"
            ("Move" (
                     ("u" org-up-element "Up element")
                     ("d" org-down-element "Down element")
                     ("b" org-next-block "Next block"))
             "Actions" (
                        ("i" org-insert-structure-template "Insert template")
                        ("e" org-export-dispatch "Export")
                        ("t" org-todo "Toggle todo states")
                        ("T" org-babel-tangle "Tangle code")
                        ("c" org-babel-execute-src-block "Run block")
                        ("x" org-toggle-checkbox "Toggle check"))
             "Show" (
                     ("," outline-show-children "Show node children")
                     ("." org-tree-to-indirect-buffer "Edit in buffer")
                     ("f" org-global-cycle "Fold all toggle")
                     ("'" org-edit-special "Code buffer"))))
#+END_SRC
