;;; em init --- Minimalistic emacs init script to bootstrap setting from org files
;;; Commentary: standard init script to bootstrap org config loading
;;(setq inhibit-startup-message t)
(setq user-init-file (or load-file-name
                         (buffer-file-name)))
(setq user-emacs-directory (file-name-directory user-init-file))
(setq recentf-save-file (expand-file-name "recentf" user-emacs-directory))
(setq custom-file (concat user-emacs-directory "custom.el"))
;; Increase garbage collection usage to speed up startup
;; reduce later on
(setq gc-cons-threshold 10000000)
                                        ;(setq gc-cons-threshold 64000000)

;; Make sure newer bytecode is loaded
;;seems to cause issues in emacs 28.2
(setq load-prefer-newer t)

;;Handy when debugging startup
;;(setq debug-on-error t)

;; Setup fundamentally required packages to make setup work.
;;(require 'package)

;; Bootstrap package sources and setup
(setq package-install-upgrade-built-in t)
(setq package-enable-at-startup nil)
(setq package-archives (list))
(add-to-list 'package-archives '("elpa" . "https://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("elpa-nongnu" . "https://elpa.nongnu.org/nongnu/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa3" . "https://www.mirrorservice.org/sites/stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
;;(package-initialize)

;; Bootstrap straight package manager
(defvar bootstrap-version)
(setq straight-check-for-modifications 'live)
(setq straight-use-package-by-default t)
(setq straight-built-in-pseudo-packages '(emacs nadvice python image-mode project flymake xref))
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'org)

(setq load-path
      (append load-path
              (list (expand-file-name "straight/repos/org/lisp")
                    (expand-file-name "straight/repos/org/contrib/lisp"))))


;; use package support
(straight-use-package 'use-package)


;; Bootstrap `use-package'
;; (unless (package-installed-p 'use-package)
;;  (package-refresh-contents)
;;  (package-install 'use-package))


;; instal org as early as possible and load
;;(use-package org :straight t)
;;(require 'org)
;;use package does not support git so lets pull in quelpa to.
;;(use-package quelpa :straight t)
;;(use-package quelpa-use-package :straight t)


;; temporary to help with debugging
(defun check-define-key (fun keymap key def)
  (when (and (eq keymap org-mode-map)
             (eq def 'scroll-up-command))
    (debug nil "binding scroll-up-command in org-mode"))
  (apply fun keymap key def))
;;(advice-add 'define-key :around #'check-define-key)


;; Use package setup so now lets switch to our custom loader
;; (quelpa '(em-loader :fetcher gitlab
;;                     :repo "emacs-open-source-library-collection/em-loader"
;;                     :branch "master"))

(use-package em-loader
  :straight (em-loader :repo "emacs-open-source-library-collection/em-loader"
                       :host gitlab))

;; This will loop through the bundles folder executing the org files.
(em-loader-load-configs)
(em-loader-info "Finished loading org configs")
(load-file "./init-em.el")

(setq gc-cons-threshold 10000000)

;; We are not using standard init so trigger these hooks manually
;; loading with -q disables these hooks
(em-loader-info "Run init hooks")
(run-hooks 'after-init-hook 'delayed-warnings-hook)
(run-hooks 'emacs-startup-hook 'term-setup-hook)
(run-hooks 'window-setup-hook)

;;; init.el ends here
(put 'narrow-to-region 'disabled nil)
