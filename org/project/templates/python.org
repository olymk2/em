#+TITLE: Python template

#+NAME: Template constant Substitution
| PROJECT_NAME | DEMO               |
| TEST2        | 123                |
| DOMAIN       | http://example.com |


#+BEGIN_SRC emacs-lisp :results silent :tangle 123.txt
(ob-project-templates-fetch-tangle-blocks "python")
#+END_SRC

#+BEGIN_SRC emacs-lisp :results silent  
(message "%s" (or (nil) (list 1 2)))

#+END_SRC

#+BEGIN_SRC emacs-lisp :results silent  
(let ((user nil))
  (goto-char (point-min)) 
    (re-search-forward org-table-any-line-regexp nil t) 
    (if user 
        (setq replacements (mapcar (lambda (row) 
                                     (list (car row) (read-string (format "Value for %s:" (car row)) 
                                                             (first (cdr row))))) 
                                   (org-table-to-lisp))) 
      (setq replacements (org-table-to-lisp)))
    (message "%s" replacements))

#+END_SRC


* Python template

** env files
#+BEGIN_SRC sh :results silent :tangle env.sample :mkdirp yes
  NAME=<<PROJECT_NAME>>
  DATABASE_NAME=
  DATABASE_USERNAME='demo'
  DATABASE_PASSWORD='demo'
  DOMAIN=<<DOMAIN>>
#+END_SRC

#+BEGIN_SRC sh :results silent :tangle requirements.txt :mkdirp yes
django
#+END_SRC

#+BEGIN_SRC python :results silent :tangle main.py :mkdirp yes
print("hello")
tmp=x
#+END_SRC

** Docker files
*** Docker compose
#+BEGIN_SRC sh :results silent :tangle docker-compose.yml :mkdirp yes
version: '2'

volumes:
  sockets: {}

services:
  postgres:
    image: postgres:10
    volumes:
      - postgres_data_dev:/var/lib/postgresql/data
      - postgres_backup_dev:/backups
    env_file: .env
django:
    build:
      context: .
      dockerfile: Dockerfile
    command: /start-dev.sh
    depends_on:
      - postgres
    env_file: .env
    volumes:
      - .:/app

#+END_SRC

** Django dockerfile
Setup Docker file
#+BEGIN_SRC sh :results silent :tangle Dockerfile :mkdirp yes
FROM python:3.6
ENV PYTHONUNBUFFERED 1

# Requirements have to be pulled and installed here, otherwise caching won't work
COPY ./requirements /requirements
#+END_SRC
** dir-locals
#+BEGIN_SRC emacs-lisp :results silent :tangle .dir-locals :mkdirp yes
((nil . ((python-shell-virtualenv-root . "/docker:root@<<PROJECT_NAME>>_django_1:/usr/local") 
         (python-shell-interpreter . "python"))))
((nil . ((eval . (progn (add-to-list 'exec-path (concat (locate-dominating-file default-directory
                                                                                ".dir-locals.el")
                                                        "node_modules/.bin/")))))))
#+END_SRC
** Readme file

#+BEGIN_SRC sh :tangle readme.org :mkdirp yes
,* Setup
,** Building you containers
To build your container and pull in all requirements run the following
,#+BEGIN_SRC shell :results silent
docker-compose build
,#+END_SRC
,** Starting your containers
To start up your container run the following command to start in the background
,#+BEGIN_SRC shell
docker-compose up -d
,#+END_SRC
#+END_SRC

#+RESULTS:


